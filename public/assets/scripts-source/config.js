$( document ).ready(function() {

    var idLista; 
	
    // Função de listagem de menu e categorias
    function listagemMenu() {
        $.ajax({
            url: "api/V1/categories/list",
            type: "GET",
            dataType: "json",
            cache: false,
            contentType: 'application/json; charset=utf-8',
    
            success: function(data) {
                var menu = data;
    
                $.each(menu.items, function(index, value) {
                    // Listando menu e categoria
                    $(".navbar-nav, .main__filters--categories").append('' + 
                        '<li class="nav-item header__nav--item">' + 
                            '<a href="#" data-id="http://localhost:8888/api/V1/categories/' + value.id + '"class="nav-link header__nav--link" title="' + value.name + '">' + value.name + '</a>' +
                        '</li>' +    
                    ''); 
                });  
    
            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log("ERROR");
            }, 
        });
    }

    // Montando menu 
    listagemMenu();

    // Exibindo produtos após o clique no menu   
    $(document).on('click', ".header__nav--link", function (e){
        e.preventDefault();
        idLista = $(this).attr("data-id");

        // Altero breadcrumb e titulo
        $(".main__breadcrumb--active, .main__produtos--title").html($(this).attr("title"));

        // Removendo produtos existentes
        $(".main__produtos--produto").remove();

        // Exibindo produtos
        listagemProdutos(idLista);

    }); 
 

    // Função de listagem de  produtos
    function listagemProdutos (id) {
        $.ajax({
            url: id,
            type: "GET",
            dataType: "json",
            cache: false,
            contentType: 'application/json; charset=utf-8',
    
            success: function(data) {
                var data = data;
                // Montando produtos
                $.each(data.items, function(index, value) {
                    $("#listagemProdutos").append('' +
                        '<div class="main__produtos--produto col-md-3 col-lg-3 col-6">' +
                            '<div class="img">' +
                                '<picture>' +
                                    '<source class="img-fluid" data-srcset="assets/' + value.image.replace("jpg", "webp") + ' "type="image/webp">' +
                                    '<source class="img-fluid" data-srcset="assets/' + value.image + '"type="image/jpg">' +
                                    '<img class="img-fluid" src="assets/' + value.image + '"alt="' + value.name + '">' +
                                '</picture>' +
                            '</div>' +
                            '<p class="title">' + value.name + '</p> ' +
                            '<p class="price">' + value.price.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'}) + '</p> ' +
                            '<a href="#" class="buy" title="Comprar">Comprar</a> ' +
                        '</div>' +
                    ''); 
                 });  
            },
            error : function(jqXHR, textStatus, errorThrown) {
                console.log("ERROR");
            }, 
        });
    }   

    // Listando produtos padrão
    listagemProdutos("http://localhost:8888/api/V1/categories/1"); 

});
