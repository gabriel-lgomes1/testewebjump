# Instalação
-------------------
Certifique-se que tenha a ultima versão do NodeJS e NPM, e então, execute o comando abaixo
```
npm install
```
### Como editar
Após o comando acima rode o comando gulp, este comando irá compilar o css e javascript,
e ficará assistindo todas as alterações nos diretórios e executará uma tarefa de acordo com a alteração
```
gulp
```

### Visualizar o projeto
Na raiz do projeto execute o comando abaixo
```
npm start
```
 
### Tecnologias usadas
```
Gulp -      Para o uso de plugins facilitadores no desenvolvimento
```
Sass -      Para uma melhor escrita de css e possibilidades de funções e varáveis para automação
```
Webp -      Para um melhor performance no site 
```
Uglify -    Para mificar os arquivos e trazer mais segurança para a aplicação
```
Bootstrap - Para adotar um padrão de classes e facilitar a responsividade
```
jquery    - Para uma menor escrita que o javascript vanilla, utilizado para o consumo da api.
``` 